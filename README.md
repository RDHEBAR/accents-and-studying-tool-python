### Welcome to SATS, or 'Spanish Accents and Studying Tool!' ###

My name is Ravi Dhebar and I'm going to tell you a bit about my humble little side project.

SATS is the brainchild of a small cs project for a project course I took. It's nothing major and is far from complete.

This tool is a small guide into the wide world of the Spanish language. This program will provide you with quick accent marks to use and a small advanced level excerpt for reading comprehension.

Vocab games will be implemented and I will be updating the readings database with a more varied difficulty level.

***
I am part of a shared repository locked by my CS professor, I will be uploading my local homework files (in java) once all 10 weeks are completed (mid march)