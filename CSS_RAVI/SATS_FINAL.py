# Ravi Dhebar
# CSS Project
# Studying & Accents Tool (Spanish) 
# 
#
from tkinter import *
from tkinter import Tk, StringVar, ttk
from tkinter.ttk import *
from tkinter import font
import datetime
import time



#newFont = font.Font(family ='Helvetica', size = 12, weight = 'italics')

class SATS():

    def __init__(self):


        self.window = Tk()
        self.window.title('Studying & Accents Tool (Spanish)')
        #frame = Frame(window)
        #self.frame = Frame(self.window)
        #self.frame.grid(row = 1, column = 0)
        self.v1 = StringVar()
        self.v2 = StringVar()
        self.v3 = StringVar()
        self.v4 = StringVar()
        self.v5 = StringVar()
        self.v6 = StringVar()
        self.v7 = StringVar()
        self.v8 = StringVar()
        self.v9 = StringVar()
        
        self.header = Label(self.window, text = 'S&AT **FREE TRIAL**', justify = 'center')
        self.header.grid(row = 0, column = 0)
        self.Check = Checkbutton(self.window, text = 'Yes, I agree to this free 30-day trial.')
        self.Check2 = Checkbutton(self.window, text = 'No, I do not agree to this free 30-day trial.')
       
        self.Check.bind('<ButtonRelease-1>', self.prebegin)
        self.Check2.bind('<ButtonRelease-1>', self.ABANDON_SHIP)
        self.Check2.grid(row = 2, column = 0)
        self.Check.grid(row = 1, column = 0)
 

        #self.place = Label(self.window, text = "     ")
        #self.place.grid(row = 0, column = 12)



        


        self.languages = ['Spanish (available)', 'French (unavailable', 'German (unavailable)']
        #self.langlabels = dict((value, Label(self.window, text = value)) for value in self.languages)
        self.spn = 'Spanish(available)'
        self.fra = 'French (unavailable)'
        self.ger = 'German (unavailable)'

        self.langchoice1 = StringVar()
        self.langchoice1.set(self.spn)
        self.langchoice2 = StringVar()
        self.langchoice2.set(self.fra)
        self.langchoice3 = StringVar()
        self.langchoice3.set(self.ger)       
        
        
        self.box_value = StringVar()
        self.newFont = font.Font(family ='Helvetica', size = 9, weight = 'normal', slant = 'italic')
        self.newFont2 = font.Font(family ='Verdana', size = 14, weight = 'bold')

        #print(self.languages[0])
        

        #for value in self.langlabels:
         #   self.langlabels[value].grid(row = 2, column = 1)
       #if '<<ComboboxSelected>>' == self.langchoice1:
        #    print(True)

        #print(self.langchoice2)

        
        #Menu = Menubutton(self.window, text = 'Begin')
        #Menu.grid(row = 3, column = 0)

        self.window.mainloop()
    #def start(self, event):
        #Check = Checkbutton(self.window, text = 'Test')
        #Check.grid(row = 0, column = 9)
        #Check.bind('ButtonRelease-1>', self.test)
        #Check2 = Checkbutton(self.window, text = 'Test2')
        #Check2.grid(row = 4, column = 0)
        
    #def test(self, event):
        #newButton = Button(self.window, text = 'Button #2')
        #newButton.grid(row = 1, column = 10)

    def prebegin(self, event):
       # self.signlabel.destroy()
        self.Check2.destroy()
        self.signlabel = Button(self.window, text = '   Sign   ')
        self.signlabel.bind('<ButtonRelease-1>', self.begin)
        self.signlabel.grid(row = 3, column = 2)
        self.disclaimer = Entry(self.window, textvariable = self.v9)
        self.disclaimer.grid(row = 3, column = 0)
        self.name = self.disclaimer.get()
        
    def ABANDON_SHIP(self, event):
        self.window.destroy()
        print('you suck')
        
    def TextExercise(self, event):
        content = open('Spanish_Excerpts.txt', "r")
        
        self.Text = Text(self.window, height = 35, width = 45)
        self.Text.insert(END, content.read())
        self.Text.grid(row = 20, column = 0)

    def begin(self, event):
       # self.time = datetime.time()

        self.timee = datetime.time()
        self.signlabel.destroy()
        self.disclaimer.destroy()
        self.Check.destroy()
        
        self.logo = PhotoImage(file = 'logo.png')
        self.labelLogo = Label(self.window, image = self.logo)
        self.labelLogo.grid(row = 0, column=0)

        self.loading= Label(self.window, font = self.newFont2, text = 'Loading application...')
        self.extra = Button(self.window, text = 'Click to begin!')
        self.extra.grid(row = 5, column = 0)
        self.extra.bind('<ButtonRelease-1>', self.stage3begin)
        self.loading.grid(row = 4, column = 0)
        
        
 #       self.stage3begin()
 
        #while datetime.time() - self.time > 3:
         #   self.stage3begin()
        
        #self.labelLogo.destroy()
       # self.currentChoice = self.LangSelect.current()
        #if self.currentChoice != -1:
           # for each in self.langlabels.self.languages():
              #  each.config(relief='flat')
            #value = values[self.currentChoice]
           # each = labels[value]
         #   each.config(relief = 'raised')
    def trueBegin(self, event):
        self.ButtonTwo.bind('<ButtonRelease-1>', self.TextExercise)
        self.Begin.bind('<ButtonRelease-1>', self.accents)
        
    def stage3begin(self, event):
        self.extra.destroy()
        self.labelLogo.destroy()
        self.loading.destroy()
        
        self.username = self.v9.get()
        self.nombre = Label(self.window, text = "User: "+self.username+"                           Version 1.0")
        self.nombre.grid(row = 0, column = 10)
        self.LangSelect = Combobox(self.window, font = self.newFont, values = self.languages, state = 'readonly', justify = 'center')        
        self.LangSelect.bind("<<ComboboxSelected>>", self.trueBegin)
        self.LangSelect.grid(row = 1, column = 0)

        self.Begin = Button(self.window, text = "Show accents table")
        self.Begin.grid(row = 3, column = 0)

        
        self.ButtonTwo = Button(self.window, text = 'Show reading exercises')        

        self.ButtonTwo.grid(row = 4, column = 0)               

    def accents(self, event):

        #
        self.a = Button(self.window, text = ' á ', width = 4)
        self.a.grid(row = 5, column = 6)
        self.aa = Entry(self.window, textvariable = self.v1, width = 3)
        self.aa.grid(row = 5, column = 7)
        self.a.bind('<ButtonRelease-1>', self.addA)

        self.e = Button(self.window, text = ' é ', width = 4)
        self.e.grid(row = 6, column = 6)
        self.ee = Entry(self.window, textvariable = self.v2, width = 3)
        self.ee.grid(row =6, column = 7)
        self.e.bind('<ButtonRelease-1>', self.addE)
        
        self.i = Button(self.window, text = ' í ', width = 4)
        self.i.grid(row = 7, column = 6)
        self.ii = Entry(self.window, textvariable = self.v3, width = 3)
        self.ii.grid(row = 7, column = 7)
        self.i.bind('<ButtonRelease-1>', self.addI)

        self.o = Button(self.window, text = ' ó ', width = 4)
        self.o.grid(row = 8, column = 6)
        self.oo = Entry(self.window, textvariable = self.v4, width = 3)
        self.oo.grid(row = 8, column = 7)
        self.o.bind('<ButtonRelease-1>', self.addO)
        
        self.u = Button(self.window, text = ' ú ', width = 4)
        self.u.grid(row = 7, column = 8)
        self.uu = Entry(self.window, textvariable = self.v5, width = 3)
        self.uu.grid(row = 7, column = 9)
        self.u.bind('<ButtonRelease-1>', self.addU)

        self.n = Button(self.window, text = ' ñ ', width = 4)
        self.n.grid(row = 8, column = 8)
        self.nn = Entry(self.window, textvariable = self.v6, width = 3)
        self.nn.grid(row = 8, column = 9)
        self.n.bind('<ButtonRelease-1>', self.addN)

        self.qm = Button(self.window, text =' ¿ ', width = 4)
        self.qm.grid(row =5, column = 8)
        self.qmqm = Entry(self.window, textvariable = self.v7, width = 3)
        self.qmqm.grid(row = 5, column = 9)
        self.qm.bind('<ButtonRelease-1>', self.addQM)

        self.em = Button(self.window, text =' ¡ ', width = 4)
        self.em.grid(row =6, column = 8)
        self.emem = Entry(self.window, textvariable = self.v8, width = 3)
        self.emem.grid(row = 6, column = 9)
        self.em.bind('<ButtonRelease-1>', self.addEM)

    def addA(self, event):
        self.v1.set(str('á'))

    def addE(self, event):
        self.v2.set(str('é'))

    def addI(self, event):
        self.v3.set(str('í'))


    def addO(self, event):
        self.v4.set(str('ó'))
                                
    def addU(self, event):
        self.v5.set(str('ú'))
                
    def addN(self, event):
        self.v6.set(str('ñ'))


    def addQM(self, event):
        self.v7.set(str('¿'))

    def addEM(self, event):
        self.v8.set(str('¡'))
        

                        
class App:
    def __init__(self, parent):
        self.parent = parent
        self.value_of_combo = 'X'
        self.combo()

    def newselection(self, event):
        self.value_of_combo = self.box.get()
        print(self.value_of_combo)

    def combo(self):
        self.box_value = StringVar()
        self.box = ttk.Combobox(self.parent, textvariable=self.box_value)
        self.box.bind("<<ComboboxSelected>>", self.newselection)
        # ...
def test():
    master = Tk()

    Label(text="one").pack()

    separator = Frame(height=4, bd=2, relief=SUNKEN)
    separator.pack(fill=X, padx=5, pady=5)

    Label(text="two").pack()

    mainloop()

def test2():
     f = Frame(window, height = 32, width = 32)
     f.pack_propagate(0)
     f.pack()
     b = button(f, text = 'Sure!')
     b.pack(fill = BOTH, expand = 1)


