from tkinter import *
from tkinter import Tk, StringVar, ttk
from tkinter.ttk import *
from tkinter import font


values = ['car', 'house', 'computer']
root = Tk()
labels = dict((value, Label(root, text=value)) for value in values)

def handler(event):
    current = combobox.current()
    if current != -1:
        for label in labels.values():
            label.config(relief='flat')
        value = values[current]
        label = labels[value]
        label.config(relief='raised')

combobox = ttk.Combobox(root, values=values)
combobox.bind('<<ComboboxSelected>>', handler)
combobox.pack()
for value in labels:
    labels[value].pack()

root.mainloop()
