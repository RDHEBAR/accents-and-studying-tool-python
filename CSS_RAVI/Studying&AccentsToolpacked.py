from tkinter import *
from tkinter import Tk, StringVar, ttk
from tkinter.ttk import *
languages = ['Spanish', 'French', 'German']

class SATS():
    def __init__(self):
        
        self.window = Tk()
        self.window.title('Studying & Accents Tool (Spanish)')

        self.logo= PhotoImage(file = 'logo.png')
        self.labelLogo = Label(self.window, image = self.logo, relief = SUNKEN, anchor = W)
        self.labelLogo.pack(fill = BOTH, expand = True)


        self.box_value = StringVar()
        self.LangSelect = Combobox(self.window, textvariable = self.box_value, values = languages, justify = 'center')
        self.LangSelect.pack(side=LEFT)

        self.Begin = Button(self.window, text = 'Begin', width = 5)
        self.Begin.pack(side=LEFT)
        self.Begin.bind('<ButtonRelease-1>', self.accents)



        self.window.mainloop()


    def start(self, event):
        Check = Checkbutton(self.window, text = 'Test')
        Check.pack(side=LEFT)


    def accents(self, event):

        self.a = Button(self.window, text = ' á ', width = 4)
        self.a.pack(side= LEFT)
        self.aa = Entry(self.window, textvariable = self.a, width = 3)
        self.aa.pack(side=LEFT)

        self.e = Button(self.window, text = ' é ', width = 4)
        self.e.pack(side=LEFT)
        self.ee = Entry(self.window, textvariable = self.e, width = 3)
        self.ee.pack(Side=LEFT)

        self.i = Button(self.window, text = ' í ', width = 4)
        self.ii = Entry(self.window, textvariable = self.i, width = 3)

        self.o = Button(self.window, text = ' ó ', width = 4)
        self.oo = Entry(self.window, textvariable = self.o, width = 3)

        self.u = Button(self.window, text = ' ú ', width = 4)
        self.uu = Entry(self.window, textvariable = self.u, width = 3)

        self.n = Button(self.window, text = ' ñ ', width = 4)
        self.nn = Entry(self.window, textvariable = self.n, width = 3)

        self.qm = Button(self.window, text =' ¿ ', width = 4)
        self.qmqm = Entry(self.window, textvariable = self.qm, width = 3)

        self.em = Button(self.window, text =' ¡ ', width = 4)
        self.emem = Entry(self.window, textvariable = self.em, width = 3)
        
        
